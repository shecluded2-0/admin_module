<?php
include("../common/header.php");
error_reporting(0);

$id = $_GET['id'];
$dump = explode(":",base64_decode($_GET['data']));

$title = $dump[0];
$rate = $dump[1];
$desc = $dump[2];

if(isset($_POST['updateme']))
{
    $rate = $_POST['interest_rate'];
    $desc = $_POST['desc'];
    $title = $_POST['title'];
    $e = 0;

    if(empty($title) || empty($rate) || empty($desc))
    {
        $e = 1;
        $emsg = "Kindly provide all fields with valid data";
    }

    if($e == 0)
    {
        //saving . 
    $resp = curl_get(array("name" => $title, "interestRate" => floatval($rate), "description" => $desc, "loanPurpose" => $id),$global_var->base_url."/loan/update-loan-purpose","post",$global_var->getToken());
if($resp->success != "true")
{
    $e = 1;
    $emsg = "Error : Sever rejected your request"; 
  
}else
{

$e = 0;

?>
<script>alert("Purpose Data has been updated.");</script>
<meta http-equiv="refresh" content="0;url=../uber/metaloan" />
<?php
die();

$title = "";
$rate = "";
$desc = "";
}

    }
}

?>

       
            <!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="#">Admin Panel</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i>
                   <a href="../uber/metaloan" >Manage Loan Purpose</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i>
                   <a href="" class="breadcrumb--active">Edit Record</a> </div>
                    <!-- END: Breadcrumb -->
                 
                    <!-- BEGIN: Notifications -->
                    <?php 
                    echo notification();
                    ?>
                    <!-- END: Notifications -->
                      <!-- BEGIN: Account Menu -->
                      <?php echo account_notifications(); ?>
                    <!-- END: Account Menu -->
                </div>
                <!-- END: Top Bar -->
                <h2 class="intro-y text-lg font-medium mt-10">
                Update Loan Purpose
                   
                </h2>
                
                    <!-- start hidden section -->
                    <form action="../uber/editloanpurpose?id=<?php echo $id; ?>" method="post">
                    <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <!-- BEGIN: Basic Select -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Update Info 
                                </h2>
                                <?php
                    if(isset($_POST['updateme']) && ($e == 1))
                    {
                        ?><div class="alert alert-danger-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> <?php echo $emsg; ?> </div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($_POST['updateme']) && ($e == 0))
                    {
                        ?><div class="alert alert-success-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> <?php echo $emsg; ?> </div>
                    <?php
                    }
                    ?>
                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                  
                                </div>
                            </div>
                            <div id="basic-select" class="p-5">
                                <div class="preview">
                                    <!-- BEGIN: Basic Select -->
                                    <div>
                                      
                                        <div class="mt-2">
                                        <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $title; ?>">
                                        </div>
                                     
                                    </div>

                                    <div>
                                       
                                        <div class="mt-2">
                                        <input type="text" class="form-control" placeholder="desc" name="desc" value="<?php echo $desc; ?>">
                                        </div>
                                     
                                    </div>

                                    <div>
                                       
                                       <div class="mt-2">
                                       <input type="text" class="form-control" placeholder="Interest rate , eg 4.5" name="interest_rate" value="<?php echo $rate; ?>">
                                       </div>
                                    
                                   </div>
                                   <div>
                                       
                                       <div class="mt-2">
                                   <button name="updateme" class="btn btn-primary mt-5">Update Record</button>
                                   </div>
    </form>
                                  
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                      <!-- end hidden section -->

                </div>
 <!-- BEGIN: Delete Confirmation Modal -->
                <div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">
                                        Do you really want to delete these records? 
                                        <br>
                                        This process cannot be undone.
                                    </div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-danger w-24">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Delete Confirmation Modal -->
            </div>
            <!-- END: Content -->
        </div>
       
        <!-- BEGIN: JS Assets-->

      
        <script src="../js/app.js"></script>
        <!-- END: JS Assets-->
    </body>
</html>