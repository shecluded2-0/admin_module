<?php
include("../common/header.php");
error_reporting(0);



if(isset($_POST['createme']))
{
  
    $desc = $_POST['desc'];
    $title = $_POST['title'];
    $e = 0;

    if(empty($title) || empty($desc))
    {
        $e = 1;
        $emsg = "Kindly provide all fields with valid data";
    }
    if($e == 0)
    {
        //saving . 
    $resp = curl_get(array("name" => $title, "description" => $desc),$global_var->base_url."/target-saving/create-target-saving-category","post",$global_var->getToken());
if($resp->success != "true")
{
    $e = 1;
    $emsg = "Error : Sever rejected your request"; 
  
}else
{

$e = 0;
$emsg = "Goal successfully saved";

$title = "";

$desc = "";
}

    }
}
?>

       
            <!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="">Admin Panel</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i>
                   <a href="#" class="breadcrumb--active">Saving Goals</a> </div>
                    <!-- END: Breadcrumb -->
                 
                    <!-- BEGIN: Notifications -->
                    <?php 
                    echo notification();
                    ?>
                    <!-- END: Notifications -->
                      <!-- BEGIN: Account Menu -->
                      <?php echo account_notifications(); ?>
                    <!-- END: Account Menu -->
                </div>
                <!-- END: Top Bar -->
                <h2 class="intro-y text-lg font-medium mt-10">
                Manage Savings Goals
                   
                </h2>
                <?php
               // echo "<div align=\"right\"><a href=\"../insure/newplan\"><button class=\"btn btn-primary w-24 mr-1 mb-2\">New Purpose</button></a></div>";
                
                ?>    <!-- BEGIN: Data List -->
                
                    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
                  <?php
                  
                    if($_GET['action'] == "toggle")
{
    //update requested . 
    $resp = curl_get(array("id" => $_GET['id']),$global_var->base_url."/target-saving/change-target-saving-category-status","post",$global_var->getToken());
    if($resp->success != "true")
    {
        ?><br/>
        <div class="alert alert-danger-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Status Update Failed.. Contact Support </div>
<?

    }else {
        ?><br/>
        <div class="alert alert-success-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Status Updated </div>
<?
    }

}

?>

                        <table class="table table-report -mt-2">
                            <thead>
                                <tr>
                                   
                                    <th class="whitespace-nowrap">GOAL</th>
                                    <th class="whitespace-wrap">DESCRIPTION</th>
                                      <th class="whitespace-nowrap">CREATED</th>
                                
                                    <th class="whitespace-nowrap">STATUS</th>
                                    <th class="text-center whitespace-nowrap">ACTIONS</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php
$e = 0;

$resp = curl_get("",$global_var->base_url."/target-saving/get-target-saving-categories","get",$global_var->getToken());
if($resp->success != "true")
{
  $e = 1;
  if($resp->error == "Invalid token")
  login();
   else 
  fatal_error("Enpoint Failure",$resp);
 
 
}

 




    foreach($resp->data->targetCategories as $category)
    {
?>
                                <tr class="intro-x">
                                   
                                    <td class="font-medium whitespace-nowrap"><?php echo $category->name; ?>
                                      </td>
                                    <td class="font-medium whitespace-wrap"><?php echo $category->description; ?></td>
                                    <td class="font-medium whitespace-nowrap"><?php echo $category->updatedAt; ?></td>
                           

                                    <td class="font-medium whitespace-nowrap">
                              <?php     
                               if($category->status == "active")
                echo "<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#delete-confirmation-modal-$category->id\"><button class=\"btn btn-sm btn-outline-success w-24 inline-block mr-1 mb-2\">active</button></a>";
                else
                echo "<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#delete-confirmation-modal-$category->id\"><button class=\"btn btn-sm btn-outline-danger w-24 inline-block mr-1 mb-2\">".$category->status."</button></a>";
               
?>
                                    </td>
                                  
                                    <td class="table-report__action w-56">
                                        <div class="flex justify-center items-center">
                                           <!-- <a class="flex items-center mr-3" href="../uber/goaledit?id=<?php echo $category->id;?>&data=<?php echo base64_encode($category->name.":".$category->description); ?>"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a> -->
                                        </div>
                                    </td>
                                </tr>
                                
                               <? }

?>


                               
                              
                               
                              
                              
                            </tbody>
                        </table>
                    </div>
                    <!-- END: Data List -->
                    <!-- start hidden section -->
                    <form action="../uber/savings" method="post">
                    <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <!-- BEGIN: Basic Select -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    New Saving Goal 
                                </h2>
                                <?php
                    if(isset($_POST['createme']) && ($e == 1))
                    {
                        ?><div class="alert alert-danger-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> <?php echo $emsg; ?> </div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($_POST['createme']) && ($e == 0))
                    {
                        ?><div class="alert alert-success-soft show flex items-center mb-2" role="alert"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> <?php echo $emsg; ?> </div>
                    <?php
                    }
                    ?>
                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                  
                                </div>
                            </div>
                            <div id="basic-select" class="p-5">
                                <div class="preview">
                                    <!-- BEGIN: Basic Select -->
                                    <div>
                                      
                                        <div class="mt-2">
                                        <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $title; ?>">
                                        </div>
                                     
                                    </div>

                                    <div>
                                       
                                        <div class="mt-2">
                                        <input type="text" class="form-control" placeholder="desc" name="desc" value="<?php echo $desc; ?>">
                                        </div>
                                     
                                    </div>

                                 
                                   <div>
                                       
                                       <div class="mt-2">
                                   <button name="createme" class="btn btn-primary mt-5">Create Now</button>
                                   </div>
    </form>
                                  
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                      <!-- end hidden section -->

                </div>
 <!-- BEGIN: update Confirmation Modal -->
 <?php
  foreach($resp->data->targetCategories as $category)
  {
      ?>
                <div id="delete-confirmation-modal-<?php echo $category->id; ?>" class="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">
                                        Do you really want to <?php 
                                        if($category->status == "inactive")
                                        echo "<font color=\"green\"><b>Enable</b></font> ".$category->name;
                                        else
                                        echo "<font color=\"red\"><b>Disable</b></font> ".$category->name;
                                        ?>

                                       
                                    </div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                   <a href="../uber/savings?action=toggle&id=<?php echo $category->id;?>"><button type="button" class="btn btn-danger w-24">Proceed</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- END: update Confirmation Modal -->
            </div>
            <!-- END: Content -->
        </div>
       
        <!-- BEGIN: JS Assets-->

      
        <script src="../js/app.js"></script>
        <!-- END: JS Assets-->
    </body>
</html>