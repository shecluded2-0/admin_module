<?php


include("../common/header.php");

if(isset($_POST['email_search']))
{
    $email = $_POST['email_search'];
}
if(isset($_COOKIE["token"]))
{
$token = $_COOKIE["token"];
}
?>


            <!-- BEGIN: Content -->
            <div class="content">
                <!-- BEGIN: Top Bar -->
                <div class="top-bar">
                    <!-- BEGIN: Breadcrumb -->
                    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="">Admin Users</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i>
                   <a href="#" class="breadcrumb--active">Manage  accounts</a> </div>
                    <!-- END: Breadcrumb -->
                 
                    <!-- BEGIN: Notifications -->
                    <?php 
                    echo notification();
                    ?>
                    <!-- END: Notifications -->
                      <!-- BEGIN: Account Menu -->
                      <?php echo account_notifications(); ?>
                    <!-- END: Account Menu -->
                </div>
                <!-- END: Top Bar -->
                <h2 class="intro-y text-lg font-medium mt-10 mb-10">
                    System Administrators <a href="javascript:;" data-toggle="modal" data-target="#addAdmin" class="btn btn-primary mr-1 mb-2">Add New Admin</a>
                   
                </h2>
              
                    <!-- BEGIN: Data List -->
                    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible relative">
                        <table class="table table-report -mt-2 admins" id="admins">
                            <thead>
                                <tr>
                                <th class="whitespace-nowrap">#ID</th>
                                    <th class="whitespace-nowrap">SURNAME</th>
                                    <th class="whitespace-nowrap">FIRST NAME</th>
                                    <th class="whitespace-nowrap">EMAIL</th>
                                    <th class="whitespace-nowrap">PHONE No</th>
                                    <th class="whitespace-nowrap">ROLE</th>
                                    <th class="whitespace-nowrap">STATUS</th>
                                    <th class="text-center whitespace-nowrap">ACTIONS</th>
                                </tr>
                            </thead>
                            <tbody class="tbody" id="tbody">

                            
                                
                           
                               
                              
                               
                              
                              
                            </tbody>
                        </table>

                        <div class="iam_loading">Please wait...</div>
                    </div>
                    
                    <!-- END: Data List -->
               
 <!-- BEGIN: Delete Confirmation Modal -->
                <div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">
                                        Do you really want to delete these records? 
                                        <br>
                                        This process cannot be undone.
                                    </div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-danger w-24">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Delete Confirmation Modal -->



                 <!-- BEGIN:Update Confirmation Modal -->
                 <div id="superlarge-modal-size-preview" class="modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                <div class="modal-content">
                            <div class="modal-body p-0">

                                <div class="p-5 text-center">
                                <form id="adminUpdate">
                                    <div class="text-2xl mb-5" id="name"></div>
                                   

                                   <div> 
                                       <label for="regular-form-1" class="form-label">Last Name</label> 
                                       <input id="lname" type="text" class="form-control form-control-rounded" name="last_name" placeholder=""> 
                                    </div> 
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">First Name</label> 
                                        <input id="fname" type="text" class="form-control form-control-rounded" name="first_name" placeholder="Rounded"> 
                                    </div> 
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Phone Number</label> 
                                        <input id="phone" type="text" class="form-control form-control-rounded" name="phone" placeholder="Rounded"> 
                                    </div> 
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Email</label> 
                                        <input id="email" type="email" class="form-control form-control-rounded" name="email" placeholder="Rounded">
                                        <input id="userid" type="hidden" class="form-control form-control-rounded" name="user_id" > 
                                        <input id="role" type="hidden" class="form-control form-control-rounded" name="role" >
                                    </div> 
                                   

                               
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-info w-24" id="updateBtn">Update</button>
                                </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div> 
                <!-- END: Update Confirmation Modal -->


                 <!-- BEGIN:Update Confirmation Modal -->
                 <div id="addAdmin" class="modal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                <div class="modal-content">
                            <div class="modal-body p-0">

                                <div class="p-5 text-center">
                                <form id="adminCreate">
                                    <div class="text-2xl mb-5">Add New</div>
                                   <div id="resp"></div>

                                   <div> 
                                       <label for="regular-form-1" class="form-label">Last Name</label> 
                                       <input id="lnames" type="text" class="form-control form-control-rounded" name="last_name" placeholder=""> 
                                    </div> 
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">First Name</label> 
                                        <input id="fnames" type="text" class="form-control form-control-rounded" name="first_name" placeholder=""> 
                                    </div> 
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Phone Number</label> 
                                        <input id="phones" type="text" class="form-control form-control-rounded" name="phone_no" placeholder=""> 
                                    </div> 
                                    
                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Email</label> 
                                        <input id="emails" type="email" class="form-control form-control-rounded" name="email" placeholder="">
                                        
                                    </div> 

                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Role</label> 
                                       <select class="form-control tom-select w-full" name="role" id="roles">

                                        <option value="admin">Admin</option>
                                        <option value="reviewer">Reviewer</option>
                                        </select>
                                    </div> 

                                    <div class="mt-3"> 
                                        <label for="regular-form-2" class="form-label">Account Status</label> 
                                       <select class="form-control tom-select w-full" name="role" id="status">

                                        <option value="active">Active</option>
                                        <option value="inactive">In-Active</option>
                                        </select>
                                    </div> 
                                   

                               
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-info w-24" id="createBtn">Create</button>
                                </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div> 
                <!-- END: Delete Confirmation Modal -->
            </div>
            <!-- END: Content -->
        </div>
       
        <!-- BEGIN: JS Assets-->

      
        <script src="../js/app.js"></script>
        
        <script src="../js/custom.js"></script>
       
        <!-- END: JS Assets-->
    </body>
</html>

<script>

      $(document).ready(function() {
    $(".iam_loading").show();

    function load_admina() {

    $.ajax({
                url: url+'user/fetch-admins',
                type: "GET",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                headers: {
                    "Authorization": "Bearer <?php echo $token; ?>"
                },
                success: function (data) {
                    $(".iam_loading").fadeOut();

                    var i, len = data.data.admins.length;

                    for (i = 1; i < len; i++) {
                        if(data.data.admins[i].status == "active"){
                            var sta = "checked";
                        }else{
                            var sta = "";
                        }
                       // alert("Name "+data.data.admins[i].first_name);
              $("#admins tbody").append(
                "<tr>"
                +"<td>"+i+"</td>"
                  +"<td>"+data.data.admins[i].last_name+"</td>"
                  +"<td>"+data.data.admins[i].first_name+"</td>"
                  +"<td>"+data.data.admins[i].email+"</td>"
                  +"<td>"+data.data.admins[i].phone_no+"</td>"
                  +"<td>"+data.data.admins[i].role+"</td>"
                  +'<td> <input type="checkbox" class="form-check-switch ustaDetails" '+sta+' id="'+data.data.admins[i].id+'"></td>'
                  +"<td>"
                  +'<div class="flex justify-center items-center">'
                    +'<a class="flex items-center mr-3 userInfo" type="button" data-toggle="modal" data-target="#superlarge-modal-size-preview" id="'+data.data.admins[i].id+'" data-lname="'+data.data.admins[i].last_name+'" data-fname="'+data.data.admins[i].first_name+'" data-email="'+data.data.admins[i].email+'" data-role="'+data.data.admins[i].role+'" data-phone="'+data.data.admins[i].phone+'" data-status="'+data.data.admins[i].status+'" href="#"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>'
                    +'<a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>'
                +'</div>'
                  +"</td>"
                +"</tr>" )
                    }

                },
                error: function (data) { 
                    $(".iam_loading").html("Error occured while retrieving data. Check your network and try again.");

                 }
            });
        }

    load_admina();




    //Get admin Info
$(document).on('click', '.userInfo', function() {

//e.preventDefault();


var pid = $(this).attr('id'); // get id of clicked row
var lname = $(this).attr('data-lname'); 
var fname = $(this).attr('data-fname'); 
var email = $(this).attr('data-email'); 
var status = $(this).attr('data-status'); 
var phone = $(this).attr('data-phone'); 
var role = $(this).attr('data-role'); 

$("#name").html(lname +" "+ fname);
$("#userid").val(pid);
$("#lname").val(lname);
$("#fname").val(fname);
$("#email").val(email);
$("#phone").val(phone);
$("#role").val(role);
});

//Update admin details
$(document).on('click', '#updateBtn', function() {

    var id = $("#userid").val();
    var lname = $("#lname").val();
    var fname = $("#fname").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var role = $("#role").val();


    var settings = {
  "url": url+"user/update-admin",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Authorization": "Bearer <?php echo $token; ?>",
    "Content-Type": "application/json"
  },
  "data": JSON.stringify({"user_id":id,"first_name":fname,"last_name":lname,"phone_no":phone,"role":role}),
};

$.ajax(settings).done(function (resp) {

if(resp.success == true){
    //$( "#mytable" ).load( "manage.php #mytable" );
    alert("Successfully Updated");
    location.reload();
    //load_admina();
    
}else{
    
    var i, len = resp.data.error.length;
    for (i = 1; i < len; i++) {
  alert("Response " +resp.data.error[i].msg)
    }

}
});

});




//Create new admin details
$(document).on('click', '#createBtn', function() {
var lnames = $("#lnames").val();
var fnames = $("#fnames").val();
var emails = $("#emails").val();
var phones = $("#phones").val();
var roles = $("#roles").val();
var statuss = $("#status").val();
var phones = $("#phones").val();

var settings = {
"url": url+"user/create-admin",
"method": "POST",
"timeout": 0,
"headers": {
"Authorization": "Bearer <?php echo $token; ?>",
"Content-Type": "application/json"
},
"data": JSON.stringify({"email":emails, "first_name":fnames,"last_name":lnames,"phone_no":phones,"role":roles}),
};

$.ajax(settings).done(function (resp) {
    $("#resp").html(resp);
if(resp.success == true){
//$( "#mytable" ).load( "manage.php #mytable" );
alert("Successfully Created");
location.reload();
//load_admina();

}else{
   
alert(resp.message);
/*var i, len = resp.data.error.length;
for (i = 1; i < len; i++) {
alert("Response " +resp.data.error[i].msg)
}*/

}
});

});

    }); 



$(document).ready(function(){
    $(document).on('click', '.ustaDetails', function() {
        var checkStatus = this.checked ? 1 : 0;
        var id = $(this).attr('id'); // get id of clicked row

        var settings = {
        "url": url+"user/enable-admin",
        "method": "POST",
        "timeout": 0,
        "headers": {
        "Authorization": "Bearer <?php echo $token; ?>",
        "Content-Type": "application/json"
        },
        "data": JSON.stringify({ "user_id":id }),
        };

        $.ajax(settings).done(function (resp) {
            $("#resp").html(resp);
        if(resp.success == true){
        //$( "#mytable" ).load( "manage.php #mytable" );
        alert("Successfully Updated");
        location.reload();
        //load_admina();

        }else{
        
        alert(resp.message);
        /*var i, len = resp.data.error.length;
        for (i = 1; i < len; i++) {
        alert("Response " +resp.data.error[i].msg)
        }*/

        }
        });

    });
});

   
</script>